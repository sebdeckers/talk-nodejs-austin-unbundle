# Talk: Unbundle NPM packages for frontend development

- Speaker: Sebastiaan Deckers
- Date: 2012-02-19
- Event: Austin Node.js
- RSVP: https://www.meetup.com/austinnodejs/events/hvklzqybcdbzb/

Unbundle is a tiny tool I wrote to make NPM package work on the web by simply tracing and rewriting ESM export/import paths. I'll quickly explain how it works and how it fits in my development toolchain. Demo with a real world web application that also uses Web Components and HTTP Server Push to optimise asset delivery.

https://gitlab.com/sebdeckers/unbundle

## Demo

Create `app.js` & `index.html`

`npm i unbundle http-server d3`

`npx unbundle --entry app.js --destination public`

`cp index.html public/`

`npx http-server -o`
